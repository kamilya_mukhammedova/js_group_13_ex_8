import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Quote } from '../shared/quote.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-quote-form',
  templateUrl: './quote-form.component.html',
  styleUrls: ['./quote-form.component.css']
})
export class QuoteFormComponent implements OnInit {
  category = '';
  author = '';
  text = '';
  title = '';
  quoteId = '';

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params:Params) => {
      if(params['id']) {
        this.title = 'Edit a quote';
        this.quoteId = params['id'];
        this.getQuoteData(this.quoteId);
      } else {
        this.title = 'Submit new quote';
      }
    });
  }

  getQuoteData(id: string) {
    this.http.get<Quote>(`https://kamilya-61357-default-rtdb.firebaseio.com/quotes/${id}.json`)
      .pipe(map(result => {
        return result;
      }))
      .subscribe(quote => {
        this.category = quote.category;
        this.author = quote.author;
        this.text = quote.text;
      });
  }

  saveQuote() {
    const body = {author: this.author, category: this.category, text: this.text};
    if(this.title === 'Submit new quote') {
      if(this.category !== '' && this.author !== '' && this.text !== '') {
       this.getPostRequest(body);
      } else {
        alert('You need to fill all the fields. Please try again.');
      }
    } else {
    this.getPutRequest(body);
    }
  }

  getPostRequest(body: {}) {
    this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/quotes.json', body)
      .subscribe(() => {
        void this.router.navigate(['/quotes', this.category]);
      });
  }

  getPutRequest(body: {}) {
    this.http.put(`https://kamilya-61357-default-rtdb.firebaseio.com/quotes/${this.quoteId}.json`,body)
      .subscribe(() => {
        void this.router.navigate(['/quotes', this.category]);
      });
  }
}


