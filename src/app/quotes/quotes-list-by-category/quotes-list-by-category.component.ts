import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { Quote } from '../../shared/quote.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-quotes-list-by-category',
  templateUrl: './quotes-list-by-category.component.html',
  styleUrls: ['./quotes-list-by-category.component.css']
})
export class QuotesListByCategoryComponent implements OnInit {
  quotesArray: Quote[] = [];
  title = 'All';

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    ) { }

  ngOnInit(): void {
    const allQuotes = this.getUrl('all');
    this.getQuoteByCategory(allQuotes);

    this.route.params.subscribe((params: Params) => {
      const categoryName = params['category'];
      this.title = categoryName.replace('-', ' ');
      const url = this.getUrl(categoryName);
      this.getQuoteByCategory(url);
    });
  }

  getUrl(categoryName: string) {
    if(categoryName === 'all') {
      return 'https://kamilya-61357-default-rtdb.firebaseio.com/quotes.json';
    } else {
      return `https://kamilya-61357-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="${categoryName}"`
    }
  }

  getQuoteByCategory(url: string) {
    this.http.get<{[id: string]: Quote}>(url)
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const quoteId = result[id];
          return new Quote(id, quoteId.author, quoteId.category, quoteId.text);
        })
      }))
      .subscribe(quotes => {
        this.quotesArray = quotes;
      })
  }

  deleteQuote(id: string) {
    this.http.delete(`https://kamilya-61357-default-rtdb.firebaseio.com/quotes/${id}.json`)
      .subscribe();
  }
}
