import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotesComponent } from './quotes/quotes.component';
import { QuoteFormComponent } from './quote-form/quote-form.component';
import { QuotesListByCategoryComponent } from './quotes/quotes-list-by-category/quotes-list-by-category.component';

const routes: Routes = [
  {path: 'quotes', component: QuotesComponent, children: [
      {path: '', component: QuotesListByCategoryComponent},
      {path: ':category', component: QuotesListByCategoryComponent},
    ]},
  {path: 'add-quote', component: QuoteFormComponent},
  {path: 'quote-edit/:id', component: QuoteFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
